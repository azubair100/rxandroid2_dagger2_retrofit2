package com.zubair.ahmed.musicsearch.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.squareup.otto.Bus;
import com.zubair.ahmed.musicsearch.infrastructure.MusicSearchApplication;

//Using this class to initialize the application class
// which in return sets up event bus and  leak canary for memory leaks
public class BaseActivity extends AppCompatActivity {

    protected MusicSearchApplication application;
    public static Bus bus;

    //You have to register and unregister the bus after you're done
    //otherwise it may lead to memory leaks
    //thus we register it onCreate() and unregister onDestroy()

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        application = (MusicSearchApplication) getApplication();
        bus = application.getBus();
        bus.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

}
