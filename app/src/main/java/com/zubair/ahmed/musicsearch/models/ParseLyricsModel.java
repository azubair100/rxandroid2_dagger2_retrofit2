package com.zubair.ahmed.musicsearch.models;


import com.google.gson.annotations.SerializedName;

//this class takes the lyrics from the lyrics key from the return

public class ParseLyricsModel {
    @SerializedName("lyrics")
    String lyrics;

    public ParseLyricsModel() {
    }

    public String getLyrics() {
        return lyrics;
    }
}
