package com.zubair.ahmed.musicsearch.ui.fragments.songdetails;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jean.jcplayer.JcAudio;
import com.example.jean.jcplayer.JcPlayerView;
import com.squareup.picasso.Picasso;
import com.zubair.ahmed.musicsearch.R;
import com.zubair.ahmed.musicsearch.infrastructure.MusicSearchApplication;
import com.zubair.ahmed.musicsearch.ui.activities.BaseActivity;
import com.zubair.ahmed.musicsearch.models.ParseLyricsModel;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SongDetailsFragment extends Fragment implements SongDetailsView {

    //the views needed to display information etc
    @BindView(R.id.song_details_image_view) ImageView albumArtImageView;
    @BindView(R.id.song_details_jcplayer) JcPlayerView jcPlayerView;
    @BindView(R.id.song_details_album_name) TextView albumNameTextView;
    @BindView(R.id.song_details_artist_name) TextView artistNameTextView;
    @BindView(R.id.song_details_lyrics) TextView lyricsTextView;


    //the values needed to display the information
    private String trackName;
    private String artistName;
    private String albumName;
    private String artWorkUrl;
    private String previewUrl;

    @Inject
    SongDetailsPresenter presenter;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public SongDetailsFragment() {
        // Required empty public constructor
    }

    public static SongDetailsFragment newInstance(String trackName, String artistName, String albumName, String artWorkUrl, String previewUrl) {
        SongDetailsFragment fragment = new SongDetailsFragment();
        Bundle args = new Bundle();
        args.putString("trackName", trackName);
        args.putString("artistName", artistName);
        args.putString("albumName", albumName);
        args.putString("artWorkUrl", artWorkUrl);
        args.putString("previewUrl", previewUrl);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trackName = getArguments().getString("trackName");
            artistName = getArguments().getString("artistName");
            albumName = getArguments().getString("albumName");
            artWorkUrl = getArguments().getString("artWorkUrl").replaceAll("100x100", "400x400");
            previewUrl = getArguments().getString("previewUrl");
        }
        //registering the bus here so we can receive the lyrics response
        BaseActivity.bus.register(this);
    }

    //little extra to listen to the song preview


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_song_details, container, false);
        ButterKnife.bind(this, view);

        ((MusicSearchApplication) getActivity().getApplication()).getAppComponent().inject(this);
        presenter.setView(this);


        setJCAudio(presenter.getJcAudios(trackName, previewUrl));
        setTexts();
        getLyrics();
        setAlbumArtWork();
        return view;
    }

    //un registering the bus so that we don't get memory leaks
    @Override
    public void onStop() {
        super.onStop();
        jcPlayerView.kill();
        BaseActivity.bus.unregister(this);
    }

    private void getLyrics(){
        mCompositeDisposable.add(
                presenter.getLyrics("getSong", artistName, trackName, "realjson")
                        .subscribeOn(Schedulers.io()) // "work" on io thread
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<ParseLyricsModel>() {
                            @Override
                            public void accept(final ParseLyricsModel lyrics)
                                    throws Exception {
                               setLyrics(lyrics.getLyrics());
                            }
                        })
        );
    }



    @Override
    public void setLyrics(String lyrics) {
        if(lyrics.matches("Not found")){
            lyricsTextView.setText(lyrics);
        }
        //I add the lyrics 3 times because for some reason
        //the lyrics wiki api doesn't give you all the lyrics
        //which is a shame, it makes the page look weird
        else{
            lyricsTextView.setText(lyrics + lyrics + lyrics);
        }
    }

    @Override
    public void setAlbumArtWork() {
        Picasso.with(getContext()).load(artWorkUrl)
                .error(R.drawable.art_work_default_details)
                .placeholder(R.drawable.art_work_default_details)
                .into(albumArtImageView);
    }

    @Override
    public void setJCAudio(ArrayList<JcAudio> jcAudios) {
        jcPlayerView.initPlaylist(jcAudios);
    }

    @Override
    public void setTexts() {
        albumNameTextView.setText(albumName);
        artistNameTextView.setText(artistName);
    }
}
