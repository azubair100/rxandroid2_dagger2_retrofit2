package com.zubair.ahmed.musicsearch.ui.activities.homepage;

import android.support.v4.app.Fragment;

public interface HomePageView {
    void setUpSearchButton();
    void replaceCurrentFragmentWith(Fragment newFragment);
    void hideKeyBoard();
    void onlySearchBarVisible();
    void setError();
}
