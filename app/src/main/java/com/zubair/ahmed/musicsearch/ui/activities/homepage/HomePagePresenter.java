package com.zubair.ahmed.musicsearch.ui.activities.homepage;


public interface HomePagePresenter {
    void setView(HomePageView view);
    boolean validateSearchQuery(String query);

}
