package com.zubair.ahmed.musicsearch.ui.fragments.songlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.zubair.ahmed.musicsearch.models.SongsSearchResponseModel;

import io.reactivex.Single;


public interface SongListPresenter {
    void setView(SongListView view, RecyclerView recyclerView);
    Single<SongsSearchResponseModel> returnSongList(String query);
}
