package com.zubair.ahmed.musicsearch.dagger;


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.zubair.ahmed.musicsearch.infrastructure.MusicSearchApplication;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private static final String SONG_BASE_URL = "SONG_BASE_URL";
    private static final String LYRICS_BASE_URL = "LYRICS_BASE_URL";

    @Provides
    @Named(SONG_BASE_URL)
    String provideSongBaseUrlString(){
        return MusicSearchApplication.BASE_APPLE_URL;
    }

    @Provides
    @Named(LYRICS_BASE_URL)
    String provideLyricsBaseUrlString(){
        return MusicSearchApplication.BASE_WIKI_URL;
    }


    @Provides
    @Singleton
    Converter.Factory provideGsonConverter(){
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofitSong(Converter.Factory converter, @Named(SONG_BASE_URL) String baseUrl){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converter)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();

    }


    @Provides
    @Singleton
    Retrofit provideRetrofitLyrics(Converter.Factory converter, @Named(LYRICS_BASE_URL) String baseUrl){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converter)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

}
