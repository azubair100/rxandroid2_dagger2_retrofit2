package com.zubair.ahmed.musicsearch.ui.fragments.songdetails;


import com.example.jean.jcplayer.JcAudio;
import com.zubair.ahmed.musicsearch.models.ParseLyricsModel;

import java.util.ArrayList;

import io.reactivex.Single;

public interface SongDetailsPresenter {

    void setView(SongDetailsView view);
    ArrayList<JcAudio> getJcAudios(String title, String url);
    Single<ParseLyricsModel> getLyrics(String request, String artist, String song, String type);

}
