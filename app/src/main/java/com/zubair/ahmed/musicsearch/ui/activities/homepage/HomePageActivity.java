package com.zubair.ahmed.musicsearch.ui.activities.homepage;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.zubair.ahmed.musicsearch.R;
import com.zubair.ahmed.musicsearch.ui.activities.BaseActivity;
import com.zubair.ahmed.musicsearch.ui.fragments.songlist.SongsListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

//Extends BaseActivity to access the Application class for the event bus
public class HomePageActivity extends BaseActivity implements HomePageView{

    @BindView(R.id.home_page_constraint_layout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.home_page_frame_layout)
    FrameLayout frameLayout;
    @BindView(R.id.search_image_button)
    ImageButton searchButton;
    @BindView(R.id.search_edit_text)
    EditText searchEditText;
    @BindView(R.id.search_card_view)
    CardView searchCardView;

    //Storing the search results in this array list
    //Storing the search query here

    private HomePagePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homeage);
        ButterKnife.bind(this);

        presenter = new PresenterHomePage();
        presenter.setView(this);
        //clicking feature for the search image button
        setUpSearchButton();
    }

    //view
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //If the user returns to the landing page again make the search bar visible
        onlySearchBarVisible();
    }

    //Since I only have one click able view on the screen
    //No need to implement OnClickListener on a class scale

    @Override
    public void setUpSearchButton() {
        searchButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //validate the edit text entry first and then make the http call
                        if(presenter.validateSearchQuery(searchEditText.getText().toString())){
                            replaceCurrentFragmentWith(SongsListFragment.newInstance(searchEditText.getText().toString()));
                        }
                    }
                }
        );
    }



    @Override
    public void replaceCurrentFragmentWith(Fragment newFragment) {
        searchCardView.setVisibility(View.INVISIBLE);
        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.home_page_frame_layout, newFragment).
                addToBackStack(null).
                commit();
    }


    @Override
    public void hideKeyBoard() {
        try {
            InputMethodManager imm =
                    (InputMethodManager) getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onlySearchBarVisible() {
        if (frameLayout.getChildCount() == 1) {
            searchCardView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setError() {
        searchEditText.setError(getString(R.string.nothing_typed));
    }




}
