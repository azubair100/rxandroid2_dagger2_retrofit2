package com.zubair.ahmed.musicsearch.ui.activities.homepage;


import android.text.TextUtils;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.zubair.ahmed.musicsearch.ui.fragments.songlist.SongsListFragment;
import com.zubair.ahmed.musicsearch.infrastructure.MusicSearchApplication;
import com.zubair.ahmed.musicsearch.models.ParseSongModel;
import com.zubair.ahmed.musicsearch.models.Song;
import com.zubair.ahmed.musicsearch.models.SongsSearchResponseModel;
import com.zubair.ahmed.musicsearch.network.SongApi;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PresenterHomePage implements HomePagePresenter{
    private HomePageView view;

    @Override
    public void setView(HomePageView view) {
        this.view = view;
    }

    @Override
    public boolean validateSearchQuery(String query) {
        if (TextUtils.isEmpty(query)) {
            view.setError();
            return false;
        }
        else{
            return true;
        }
    }


}
