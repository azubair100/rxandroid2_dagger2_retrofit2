package com.zubair.ahmed.musicsearch.infrastructure;


import android.app.Application;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.otto.Bus;
import com.zubair.ahmed.musicsearch.dagger.AppComponent;
import com.zubair.ahmed.musicsearch.dagger.AppModule;
import com.zubair.ahmed.musicsearch.dagger.DaggerAppComponent;

//Needed an application class so that we can set up event bus and leak canary
public class MusicSearchApplication extends Application {
    public static final String BASE_APPLE_URL = "https://itunes.apple.com/";
    public static final String BASE_WIKI_URL = "http://lyrics.wikia.com/";

    protected Bus bus;
    private AppComponent appComponent;

    public MusicSearchApplication() {
        bus = new Bus();
    }

    public Bus getBus() {
        return bus;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    protected AppComponent initDagger(MusicSearchApplication application){
        return DaggerAppComponent.builder().
                appModule(new AppModule(application)).
                build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //leak canary to check memory leaks
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }

        appComponent = initDagger(this);

        LeakCanary.install(this);

        //this sets up retrofit
    }

}
