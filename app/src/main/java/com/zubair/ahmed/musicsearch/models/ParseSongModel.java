package com.zubair.ahmed.musicsearch.models;


import com.google.gson.annotations.SerializedName;

//this class is used to extract the required keys from each song rows
public class ParseSongModel {

    @SerializedName("trackName")
    String trackName;
    @SerializedName("artistName")
    String artistName;
    @SerializedName("collectionName")
    String albumName;
    @SerializedName("artworkUrl100")
    String artWorkUrl;
    @SerializedName("previewUrl")
    String previewUrl;

    public ParseSongModel() {
    }

    public String getTrackName() {
        return trackName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getArtWorkUrl() {
        return artWorkUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }
}
