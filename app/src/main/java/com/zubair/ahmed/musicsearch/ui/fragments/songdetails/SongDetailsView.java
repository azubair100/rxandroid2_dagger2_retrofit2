package com.zubair.ahmed.musicsearch.ui.fragments.songdetails;

import com.example.jean.jcplayer.JcAudio;

import java.util.ArrayList;

public interface SongDetailsView {
    void setLyrics(String lyrics);
    void setAlbumArtWork();
    void setJCAudio(ArrayList<JcAudio> jcAudios);
    void setTexts();
}
