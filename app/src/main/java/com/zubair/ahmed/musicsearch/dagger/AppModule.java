package com.zubair.ahmed.musicsearch.dagger;

import android.content.Context;

import com.zubair.ahmed.musicsearch.infrastructure.MusicSearchApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private MusicSearchApplication application;

    public AppModule(MusicSearchApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideContext(){ return application; }
}
