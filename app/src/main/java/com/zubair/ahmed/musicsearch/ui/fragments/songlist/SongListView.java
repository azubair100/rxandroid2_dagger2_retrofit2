package com.zubair.ahmed.musicsearch.ui.fragments.songlist;


import com.zubair.ahmed.musicsearch.ui.fragments.songdetails.SongDetailsFragment;

public interface SongListView {
    void goToSongDetails(SongDetailsFragment songDetailsFragment);
    void setUpRecyclerView();
}
