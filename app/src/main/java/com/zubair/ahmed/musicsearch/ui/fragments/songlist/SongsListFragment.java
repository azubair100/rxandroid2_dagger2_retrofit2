package com.zubair.ahmed.musicsearch.ui.fragments.songlist;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zubair.ahmed.musicsearch.R;
import com.zubair.ahmed.musicsearch.infrastructure.MusicSearchApplication;
import com.zubair.ahmed.musicsearch.ui.fragments.songdetails.SongDetailsFragment;
import com.zubair.ahmed.musicsearch.models.ParseSongModel;
import com.zubair.ahmed.musicsearch.models.Song;
import com.zubair.ahmed.musicsearch.models.SongsSearchResponseModel;
import com.zubair.ahmed.musicsearch.ui.recycler_view_components.RecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class SongsListFragment extends Fragment implements
        RecyclerViewItemClickListener.OnRecyclerClickListener, SongListView {
    private static final String TAG = "SongsListFragment";

    @BindView(R.id.songs_list_search_term)
    TextView searchTextView;
    @BindView(R.id.songs_list_recycler_view)
    RecyclerView recyclerView;

    //stores the search result
    private ArrayList<Song> songs;

    //stores the search text to display on top
    private String searchText;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Inject
    PresenterSongList presenter;





    public SongsListFragment() {
        // Required empty public constructor
    }

    public static SongsListFragment newInstance( String searchText) {
        SongsListFragment fragment = new SongsListFragment();
        Bundle args = new Bundle();
        args.putString("search", searchText);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            searchText = getArguments().getString("search");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_songs_list, container, false);
        ButterKnife.bind(this, view);
        ((MusicSearchApplication) getActivity().getApplication()).getAppComponent().inject(this);
        presenter.setView(this, recyclerView, getContext());
        setUpRecyclerView();
        Log.d(TAG, "onCreateView: text is " + searchText);
//        presenter = new PresenterSongList();
        presenter.setView(this, recyclerView, getContext());
        displaySongs(searchText);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        mCompositeDisposable.clear();
        super.onDestroy();

    }

    //this method sets up recycler view components like inflating, adapter, click listeners etc
    //view


    //view
    @Override
    public void onItemClick(View view, int position) {
        //Only move to the SongDetailsFragment if the user clicked on a valid row
        if (songs.size() > 0) {
            Song song = songs.get(position);

            SongDetailsFragment songDetailsFragment
                    = SongDetailsFragment.newInstance(
                    song.getTrackName(),
                    song.getArtistName(),
                    song.getAlbumName(),
                    song.getAlbumImageUrl(),
                    song.getPreviewUrl()
            );

            //go to the songDetailsFragment
            goToSongDetails(songDetailsFragment);
        }
    }


    private void displaySongs(String query){
        mCompositeDisposable.add(
                presenter.returnSongList(query)
                        .subscribeOn(Schedulers.io()) // "work" on io thread
                        .observeOn(AndroidSchedulers.mainThread()) // "listen" on UIThread
                        .map(new Function<SongsSearchResponseModel, List<Song>>() {
                            @Override
                            public List<Song> apply(@NonNull SongsSearchResponseModel songsSearchResponseModel) throws Exception {
                                List<Song> songsList = new ArrayList<>();
                                for(ParseSongModel songModel : songsSearchResponseModel.songResult){
                                    songsList.add(
                                            new Song(
                                                    songModel.getTrackName(),
                                                    songModel.getArtistName(),
                                                    songModel.getAlbumName(),
                                                    songModel.getArtWorkUrl(),
                                                    songModel.getPreviewUrl()
                                            )
                                    );
                                }
                                Log.d(TAG, "apply: " + songsList.get(0));
                                songs = (ArrayList<Song>) songsList;
                                return songsList;
                            }
                        }).subscribe(presenter.getObserver())
        );
    }

    @Override
    public void goToSongDetails(SongDetailsFragment songDetailsFragment) {
        getActivity().
                getSupportFragmentManager().
                beginTransaction().
                replace(R.id.home_page_frame_layout, songDetailsFragment).
                addToBackStack(null).
                commit();
    }

    @Override
    public void setUpRecyclerView() {
        searchTextView.setText(getString(R.string.showing_results) + searchText + getString(R.string.ending_search_Result));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getContext(), recyclerView, this));
    }
}
