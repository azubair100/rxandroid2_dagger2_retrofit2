package com.zubair.ahmed.musicsearch.network;

import com.zubair.ahmed.musicsearch.models.ParseLyricsModel;
import com.zubair.ahmed.musicsearch.models.SongsSearchResponseModel;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface SongApi {

    @GET("api.php")
    Single<ParseLyricsModel> loadLyrics(
            @Query("func") String func,
            @Query("artist") String artist,
            @Query("song") String song,
            @Query("fmt") String format
    );

    @GET("search")
    Single<SongsSearchResponseModel> loadSongs(
            @Query("term") String term
    );

}
