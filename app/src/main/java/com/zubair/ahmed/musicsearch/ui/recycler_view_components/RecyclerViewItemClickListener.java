package com.zubair.ahmed.musicsearch.ui.recycler_view_components;


import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class RecyclerViewItemClickListener extends RecyclerView.SimpleOnItemTouchListener {

    public interface OnRecyclerClickListener {
        void onItemClick(View view, int position);

    }

    private final OnRecyclerClickListener mListener;
    private final GestureDetectorCompat mGestureDetector;


    //for override all intercept touch events,
    public RecyclerViewItemClickListener(
            Context context,
            final RecyclerView recyclerview,
            final OnRecyclerClickListener mListener) {

        //initialize the private variables
        this.mListener = mListener;
        mGestureDetector = new GestureDetectorCompat(
                context,
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        View childView = recyclerview.findChildViewUnder(e.getX(), e.getY());
                        if (childView != null && mListener != null) {
                            mListener.onItemClick(
                                    childView,
                                    recyclerview.getChildAdapterPosition(childView)
                            );
                        }
                        return true;
                    }

                });

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        if (mGestureDetector != null) {
            boolean result = mGestureDetector.onTouchEvent(e);
            return result;
        } else {
            return false;
        }
    }

}
