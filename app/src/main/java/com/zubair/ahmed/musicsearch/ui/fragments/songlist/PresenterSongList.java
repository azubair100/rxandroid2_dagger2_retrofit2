package com.zubair.ahmed.musicsearch.ui.fragments.songlist;


import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.zubair.ahmed.musicsearch.infrastructure.MusicSearchApplication;
import com.zubair.ahmed.musicsearch.models.Song;
import com.zubair.ahmed.musicsearch.models.SongsSearchResponseModel;
import com.zubair.ahmed.musicsearch.network.SongApi;
import com.zubair.ahmed.musicsearch.ui.recycler_view_components.RecyclerViewAdapter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PresenterSongList implements SongListPresenter {
    private SongListView view;
    private RecyclerView recyclerView;
    private Context mContext;
    @Inject
    SongApi songApi;

    public PresenterSongList(Context context) {
        ((MusicSearchApplication) context).getAppComponent().inject(this);
        this.mContext = context;
    }

    @Override
    public void setView(SongListView view, RecyclerView recyclerView) {
        this.view = view;
        this.recyclerView = recyclerView;
    }

    public Single<SongsSearchResponseModel> returnSongList(String query){
        Single<SongsSearchResponseModel> songs = songApi.loadSongs(query);
        return songs;
    }


    public Consumer<List<Song>> getObserver(){
        return new Consumer<List<Song>>() {
            @Override
            public void accept(
                    @io.reactivex.annotations.NonNull final List<Song> songs)
                    throws Exception {
                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(songs, mContext);
                recyclerView.setAdapter(recyclerViewAdapter);
            }
        };
    }


}
