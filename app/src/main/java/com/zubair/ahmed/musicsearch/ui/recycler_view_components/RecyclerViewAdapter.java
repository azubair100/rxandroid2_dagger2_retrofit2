package com.zubair.ahmed.musicsearch.ui.recycler_view_components;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zubair.ahmed.musicsearch.R;
import com.zubair.ahmed.musicsearch.models.Song;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//mandatory adapter class
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.SongHolder> {

    private List<Song> songs;
    private Context mContext;

    public RecyclerViewAdapter(List<Song> songs, Context mContext) {
        this.songs = songs;
        this.mContext = mContext;
    }

    //using song_row as the cell for each song object
    @Override
    public RecyclerViewAdapter.SongHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.songs_row, parent, false);
        return new SongHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.SongHolder holder, int position) {
        //if no response just add "No song found"
        if (songs == null || songs.size() == 0) {
            holder.songName.setText(R.string.no_song_return);
        }
        //if there are results, add the names and get the album art
        else {
            Song song = songs.get(position);
            holder.songName.setText(mContext.getString(R.string.track) + " " + song.getTrackName());
            holder.albumName.setText(mContext.getString(R.string.album) + " " + song.getAlbumName());
            holder.artistName.setText(mContext.getString(R.string.artist) + " " + song.getArtistName());

            Picasso.with(mContext).load(song.getAlbumImageUrl())
                    .error(R.drawable.art_work_default_rows)
                    .placeholder(R.drawable.art_work_default_rows)
                    .into(holder.albumIcon);
        }
    }

    @Override
    public int getItemCount() {
        return ((songs != null) && (songs.size() > 0) ? songs.size() : 1);
    }


    static class SongHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.songs_row_track_text_view)
        TextView songName;
        @BindView(R.id.songs_row_album_text_view)
        TextView albumName;
        @BindView(R.id.songs_row_artist_text_view)
        TextView artistName;
        @BindView(R.id.songs_row_album_image)
        ImageView albumIcon;

        SongHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
