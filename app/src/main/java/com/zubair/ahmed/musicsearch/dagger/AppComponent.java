package com.zubair.ahmed.musicsearch.dagger;



import com.zubair.ahmed.musicsearch.ui.fragments.songdetails.PresenterSongDetails;
import com.zubair.ahmed.musicsearch.ui.fragments.songdetails.SongDetailsFragment;
import com.zubair.ahmed.musicsearch.ui.fragments.songlist.PresenterSongList;
import com.zubair.ahmed.musicsearch.ui.fragments.songlist.SongsListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, PresenterModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(SongsListFragment target);
    void inject(SongDetailsFragment target);
    void inject(PresenterSongList target);
    void inject(PresenterSongDetails target);

}
