package com.zubair.ahmed.musicsearch.ui.fragments.songdetails;

import android.content.Context;

import com.example.jean.jcplayer.JcAudio;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.zubair.ahmed.musicsearch.infrastructure.MusicSearchApplication;
import com.zubair.ahmed.musicsearch.models.ParseLyricsModel;
import com.zubair.ahmed.musicsearch.network.SongApi;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PresenterSongDetails implements SongDetailsPresenter{
    private SongDetailsView view;
    @Inject
    SongApi songApi;

    public PresenterSongDetails(Context context) {
        ((MusicSearchApplication) context).getAppComponent().inject(this);
    }

    @Override
    public void setView(SongDetailsView view) {
        this.view = view;
    }

    @Override
    public ArrayList<JcAudio> getJcAudios(String title, String url) {

        ArrayList<JcAudio> jcAudios = new ArrayList<>();
        jcAudios.clear();
        jcAudios.add(JcAudio.createFromURL(title, url));
        return jcAudios;
    }

    @Override
    public Single<ParseLyricsModel> getLyrics(String request, String artist, String song, String type) {
        Single<ParseLyricsModel> lyrics = songApi.loadLyrics(request, artist, song, type);
        return lyrics;
    }

}
