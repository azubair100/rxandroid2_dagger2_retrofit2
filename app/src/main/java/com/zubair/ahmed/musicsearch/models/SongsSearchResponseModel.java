package com.zubair.ahmed.musicsearch.models;


import com.google.gson.annotations.SerializedName;

import java.util.List;

//this class is used to extract the result from the json response
public class SongsSearchResponseModel {

    @SerializedName("results")
    public List<ParseSongModel> songResult;

}
