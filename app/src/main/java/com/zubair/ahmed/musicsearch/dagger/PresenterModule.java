package com.zubair.ahmed.musicsearch.dagger;

import android.content.Context;

import com.zubair.ahmed.musicsearch.ui.fragments.songdetails.PresenterSongDetails;
import com.zubair.ahmed.musicsearch.ui.fragments.songdetails.SongDetailsPresenter;
import com.zubair.ahmed.musicsearch.ui.fragments.songlist.PresenterSongList;
import com.zubair.ahmed.musicsearch.ui.fragments.songlist.SongListPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule {

    @Provides
    @Singleton
    SongListPresenter provideSongListPresenter(Context context){
        return new PresenterSongList(context);
    }

    @Provides
    @Singleton
    SongDetailsPresenter provideSongDetailsPresenter(Context context){
        return new PresenterSongDetails(context);
    }
}
