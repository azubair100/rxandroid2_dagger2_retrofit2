package com.zubair.ahmed.musicsearch.models;

import android.os.Parcel;
import android.os.Parcelable;

//The formatted class for parsed song response
//instances of this class will be displayed inside the recycler view
//implements parcelable so that we can get pass it to fragments, activities etc.
public class Song implements Parcelable {

    public String trackName;
    public String artistName;
    public String albumName;
    public String albumImageUrl;
    public String previewUrl;

    public Song(String trackName,
                String artistName,
                String albumName,
                String albumImageUrl,
                String previewUrl) {
        this.trackName = trackName;
        this.artistName = artistName;
        this.albumName = albumName;
        this.albumImageUrl = albumImageUrl;
        this.previewUrl = previewUrl;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getAlbumImageUrl() {
        return albumImageUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    protected Song(Parcel in) {
        trackName = in.readString();
        artistName = in.readString();
        albumName = in.readString();
        albumImageUrl = in.readString();
        previewUrl = in.readString();
    }


    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel parcel) {
            return null;
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(trackName);
        parcel.writeString(artistName);
        parcel.writeString(albumName);
        parcel.writeString(albumImageUrl);
        parcel.writeString(previewUrl);
    }
}
