package com.zubair.ahmed.musicsearch;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

import com.zubair.ahmed.musicsearch.ui.activities.homepage.HomePageActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.startsWith;


@RunWith(AndroidJUnit4.class)
public class HomeActivityTest {

    @Rule
    public ActivityTestRule<HomePageActivity> mainActivityTest =
            new ActivityTestRule<>(HomePageActivity.class);

    //this test is for going to the song details fragment
    @Test
    public void triggerSongDetails(){
        onView(withId(R.id.search_edit_text)).perform(
                typeText("Tornado of souls"), closeSoftKeyboard()
        );

        setUpFragmentTransactions();

        if (getRVCount() > 0) {
            onView(withId(R.id.songs_list_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        }


    }


    //this test is for a successful search
    @Test
    public void triggerSearchWithMultiResult() {
        onView(withId(R.id.search_edit_text)).perform(
                typeText("Tornado of souls"), closeSoftKeyboard()
        );

        setUpFragmentTransactions();

        if (getRVCount() > 0) {
            onView(withId(R.id.songs_list_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        }

    }

    //this test is for a false search
    @Test
    public void triggerSearchWithNoResult() {
        onView(withId(R.id.search_edit_text)).perform(
                typeText("211F#F@#F@$RF@#@$%FG@$T#%&H#%T"), closeSoftKeyboard()
        );

        setUpFragmentTransactions();

        if (getRVCount() == 0) {
            //test here for empty cell
            onView(withId(R.id.songs_list_recycler_view))
                    .perform(RecyclerViewActions.actionOnItem(
                            hasDescendant(withText("No song found")), click()));
        }
    }

    //helper method to initiate fragment transaction
    private void setUpFragmentTransactions() {
        mainActivityTest.getActivity()
                .getSupportFragmentManager().beginTransaction();

        onView(withText(startsWith("Showing search results for")));

    }
    //helper method to check for results
    private int getRVCount() {
        Fragment fragment = mainActivityTest.
                getActivity().
                getSupportFragmentManager().findFragmentById(R.id.home_page_frame_layout);
        if (fragment instanceof SongsListFragment) {
            RecyclerView recyclerview = fragment.getView().findViewById(R.id.songs_list_recycler_view);
            return recyclerview.getAdapter().getItemCount();
        }

        return -1;

    }

}
